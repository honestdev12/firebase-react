// Customer Router Tests
const expect = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

// Create Customer Login Credentials and Profile Data ( Register Customer with Stripe as well )
describe("Creating a Customer with Login Credentials, Profile Data, and Stripe Object", function(){
    it("should create a new customer document and store it in firestore.");
    it("should update profile data of customer object");
    it("should throw an error if the customer id passed through the http request doesn't match any stored");
    it("should create a stripe customer object using .create() method on stripe.customers");
});
// Logout, revoking authorization
describe("Logout", function(){
    it("should detach user credentials from session");
});
// Update customer information
describe("Updating Customer information", function(){
    it("should return a customer object with updated object properties");
    it("should throw an error when an incorrect customer id is passed through the http request");
});
// Non-authorized navigation
describe("When a visitor navigates beyond the landing page without user authorization", function(){
    it("should throw a 403 error due to lack of authorization");
});
// Delete customer
describe("When a customer is deleted", function(){
    it("should throw an error if the customer id to delete doesn't match any within the database");
    it("should return a 200 status indicating the customer was deleted if the id matches a customer object");
    it("should return a 404 status if the customer's old id is searched for again");
});
// Read customer
describe("Retrieving a customer object", function(){
    it("should return an object when the customer id passed to the request matches on in the database");
    it("should throw an error (404) if the customer id passed does not match an entry in the database");
    it("should return a customer object containing an id, first name, last name, email, stripe id, and address");
});