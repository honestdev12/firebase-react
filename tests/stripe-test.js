// Stripe Router Tests

// Customer Object Management
describe("Stripe Router .retrieve() method called on stripe.customers", function(){
    it("should return a customer object when a successful GET request is made.");
    it("should return  subset of customer object if the customer was deleted, including a deleted property that will be true.");
    it("should throw an error when an incorrect customer ID is passed to the GET request.");
});

describe("Stripe Router .create() method called on stripe.customers", function(){
    it("should return the customer object when created on a successfull POST request.");
    it("should throw an error if an invoice payment is due and a source is not provided.");
    it("should throw an error if a non-existent plan or a non-existent or expired coupon is provided.");
});

describe("Stripe Router .update() method called on stripe.customers", function(){
    it("should return the customer object on a successful PUT request.");
    it("should throw an error when update parameters are invalid on an unsuccessful PUT request.");
});

describe("Stripe Router .del() method called on stripe.customers", function(){
    it("should return an object with a deleted parameter on successful DELETE request.");
    it("should throw an error if the customer ID is invalid on unsuccessful DELETE request.");
});

describe("Stripe Router .list() method called on stripe.customers", function(){
    it("should return an object with a data property that is an array containing all customers on successful request.");
    it("should return an array of length equal to the limit passed through the request.");
    it("should return an array of customers with exact match emails to the email passed through the request.");
    it("should return an array of customer objects");
});

// Card Payment Source Management Tests
describe("Stripe Router .createSource() method called on stripe.customers", function(){
    
});

describe("Stripe Router .retrieveCard() method called on stripe.customers", function(){

});

describe("Stripe Router .updateCard() method called on stripe.customers", function(){

});

describe("Stripe Router .deleteCard() method called on stripe.customers", function(){

});

describe("Stripe Router .listCards() methods called on stripe.customers", function(){

});

// Charge Management Tests
describe("Stripe Router .create() method called on stripe.charges", function(){

});

describe("Stripe Router .capture() method called on stripe.charges", function(){

});

describe("Stripe Router .retrieve() method called on stripe.charges", function(){

});

describe("Stripe Router .update() method called on stripe.charges", function(){

});

// Refund Management Tests
describe("Stripe Router .create() method called on stripe.refunds", function(){

});

describe("Stripe Router .list() method called on stripe.refunds", function(){

});

describe("Stripe Router .retrieve() method called on stripe.refunds", function(){

});

describe("Stripe Router .update() method called on stripe.refunds", function(){

});