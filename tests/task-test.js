// Task Router Tests

// Create Task
describe("Creating a Task with Customer ID, Address, Driveway Size, Walkway, Snow Depth, Time", function(){
    it("should create and store task info in customer's task collection upon final submission");
    it("should create and store task info in available_task upon final submission");
    it("should throw an error if any missing or invalid info in task object");
    it("should update customer's nested object within task history object");
    it("should throw an error if the customer id passed through the http request doesn't match any stored");
    it("should return a stripe customer object using .retrieve() method on stripe.customers");
});
// Read Single Task (current/live)
describe("Retrieving a task object", function(){
    it("should return an object when the task id passed to the request matches one in the database");
    it("should throw an error (404) if the task id passed does not match an entry in the database");
    it("should return a task object containing an id, customer id, address, drivewaySize, walkway, snowDepth, and time");
});
// Read Task History
describe("Retrieving a task object", function(){
    it("should return an object when the task id passed to the request matches one in the database");
    it("should throw an error (404) if the task id passed does not match an entry in the database");
    it("should return a task object containing an id, customer id, address, drivewaySize, walkway, snowDepth, and time");
});
// Update Task (add to Provider's Task Collection && set live to 0 || modify provider/tasks/...duration??receipt/status) 
describe("Updating current task", function(){
    it("should return a task object with updated object properties");
    it("should throw an error if any missing or invalid info passed through the http request");
});
// Delete Task
describe("When a task is deleted", function(){
    it("should throw an error if the task id to delete doesn't match any within the database");
    it("should return a 200 status indicating the task was deleted if the id matches a task object");
    it("should return a 404 status if the task's old id is searched for again");
});