const provider_router = require('express').Router();
const db = require('../pool/pool');

provider_router.get("/retrieveTasks/:id", async (req, res)=>{
    const id = req.params.id;
    try{
        const providerRef = await db.collection('providers').doc(id);
        const tasksRef = await providerRef.collection('tasks');
        tasksRef.get().then(collections => {
            let collectionsArr = [];
            collections.forEach(collection => {
                if (collection.exists) {
                    const data = collection.data();
                    collectionsArr.push(data);
                } else {
                    console.log('no task documents found');
                    res.status(404).send({error: "no task documents exist"});
                }
            });
            res.send(collectionsArr);
        });
    }catch(error){
        res.send(404, {error: "unable to locate customer"});
    }
});

provider_router.post("/postTask", async (req, res)=>{
    const provider = req.body.provider;
    console.log('provider', provider);
    const task = req.body.task;
    console.log('task', task);
    try{
        // if (customer){
            const providerRef = await db.collection('providers').doc(provider.id);
            const AddTaskDocReturnId = await providerRef.collection('tasks').add(task).then(async function(ref){
                task.id = ref.id;
                await providerRef.collection('tasks').doc(ref.id).set(task);
                return ref.id;
            });
            const taskRef = await providerRef.collection('tasks').doc(AddTaskDocReturnId);
            const taskData = await taskRef.get().then(snapshot => {
                return snapshot.data();
            });
            console.log('taskData:', taskData);
            if(taskData.id){
                res.sendStatus(200);
            }else{
                res.send(500, {error: "Failed to set id prop equal to document id"});
            }
        // }
    }catch(error){
        res.send(500, {error: "Failed to post task to database"});
    }
});

provider_router.put("/updateProvider", async (req, res)=>{
    const provider = req.body.provider;
    console.log('provider', provider);
    const task = req.body.task;
    console.log('task', task);
    try{
        // if (customer){
            const providerRef = await db.collection('providers').doc(provider.id);
            const taskRef = await providerRef.collection('tasks').doc(task.id);
            await taskRef.update(task).then(async function(ref){
                // task.id = ref.id;
                // await customerRef.collection('tasks').doc(ref.id).set(task);
                console.log ('in update task function');
                return ref.id;
            });
            const updatedTaskRef = await providerRef.collection('tasks').doc(taskRef);
            const updatedTaskData = await updatedTaskRef.get().then(snapshot => {
                return snapshot.data();
            });
            console.log('taskData:', updatedTaskData);
            if(updatedTaskData.id){
                res.sendStatus(200);
            }else{
                res.send(500, {error: "Failed to set id prop equal to document id"});
            }
        // }

    }catch(error){
        res.send(500, {error: "Failed to update task to database"});
    }
});

provider_router.put("/updateTask", async (req, res)=>{
    const customer = req.body.customer;
    console.log('customer', customer);
    const task = req.body.task;
    console.log('task', task);
    try{
        // if (customer){
            const customerRef = await db.collection('customers').doc(customer.id);
            const taskRef = await customerRef.collection('tasks').doc(task.id);
            await taskRef.update(task).then(async function(ref){
                // task.id = ref.id;
                // await customerRef.collection('tasks').doc(ref.id).set(task);
                console.log ('in update task function');
                return ref.id;
            });
            const updatedTaskRef = await customerRef.collection('tasks').doc(taskRef);
            const updatedTaskData = await updatedTaskRef.get().then(snapshot => {
                return snapshot.data();
            });
            console.log('taskData:', updatedTaskData);
            if(updatedTaskData.id){
                res.sendStatus(200);
            }else{
                res.send(500, {error: "Failed to set id prop equal to document id"});
            }
        // }

    }catch(error){
        res.send(500, {error: "Failed to update task to database"});
    }
});

provider_router.delete("/deleteProvider", async (req, res)=>{
    const customer = req.body.customer;
    console.log('customer', customer);
    const task = req.body.task;
    console.log('task', task);
    try{
        const customerRef = await db.collection('customers').doc(customer.id);
        const taskRef = await customerRef.collection('tasks').doc(task.id);
        await taskRef.delete();
        const confirmDelete = await taskRef.get().then(snapshot => {
            if(snapshot.exists){
                return true;
            } else {
                return false;
            }
        });
        if(!confirmDelete){
            res.sendStatus(200);
        } else {
            res.status(500).send({error: "Could not delete"});
        }

    }catch(error){
        res.send(404, {error: "Failed"});
    }
});

module.exports = provider_router;