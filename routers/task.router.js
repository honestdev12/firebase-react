const task_router = require('express').Router();
const db = require('../pool/pool');

task_router.get("/retrieveTasks/:id", async (req, res)=>{
    const id = req.params.id;
    try{
        const customerRef = await db.collection('customers').doc(id);
        const tasksRef = await customerRef.collection('tasks');
        tasksRef.get().then(collections => {
            let collectionsArr = [];
            collections.forEach(collection => {
                if (collection.exists) {
                    const data = collection.data();
                    collectionsArr.push(data);
                } else {
                    console.log('no task documents found');
                    res.status(404).send({error: "no task documents exist"});
                }
            });
            res.send(collectionsArr);
        });
    }catch(error){
        res.send(404, {error: "unable to locate customer"});
    }
});

task_router.post("/postTask", async (req, res)=>{
    const customer = req.body.customer;
    console.log('customer', customer);
    const task = req.body.task;
    console.log('task', task);
    try{
        // if (customer){
            const customerRef = await db.collection('customers').doc(customer.id);
            const AddTaskDocReturnId = await customerRef.collection('tasks').add(task).then(async function(ref){
                task.id = ref.id;
                await customerRef.collection('tasks').doc(ref.id).set(task);
                return ref.id;
            });
            const taskRef = await customerRef.collection('tasks').doc(AddTaskDocReturnId);
            const taskData = await taskRef.get().then(snapshot => {
                return snapshot.data();
            });
            console.log('taskData:', taskData);
            if(taskData.id){
                res.sendStatus(200);
            }else{
                res.send(500, {error: "Failed to set id prop equal to document id"});
            }
        // }
    }catch(error){
        res.send(500, {error: "Failed to post task to database"});
    }
});

task_router.put("/updateTask", async (req, res)=>{
    const customer = req.body.customer;
    console.log('customer', customer);
    const task = req.body.task;
    console.log('task', task);
    try{
        const customerRef = await db.collection('customers').doc(customer.id);
        const taskRef = await customerRef.collection('tasks').doc(task.id);
        await taskRef.update(task);
        res.sendStatus(200);  
    }catch(error){
        res.send(500, {error: "Failed to update task to database"});
    }
});

task_router.delete("/deleteTask", async (req, res)=>{
    const customer = req.body.customer;
    console.log('customer', customer);
    const task = req.body.task;
    console.log('task', task);
    try{
        const customerRef = await db.collection('customers').doc(customer.id);
        const taskRef = await customerRef.collection('tasks').doc(task.id);
        await taskRef.delete();
        const confirmDelete = await taskRef.get().then(snapshot => {
            if(snapshot.exists){
                return true;
            } else {
                return false;
            }
        });
        if(!confirmDelete){
            res.sendStatus(200);
        } else {
            res.status(500).send({error: "Could not delete"});
        }

    }catch(error){
        res.send(404, {error: "Failed"});
    }
});

module.exports = task_router;