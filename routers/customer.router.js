const customer_router = require('express').Router();
const Async = require('async');
const bcrypt = require('bcryptjs');
const saltRounds = 10;

const {
    check,
    validationResult
} = require('express-validator/check');

const db = require('../pool/pool');

const accountSid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
const authToken = 'your_auth_token';
const client = require('twilio')(accountSid, authToken);

const randomNumber = require('random-number');

const addCustomer = (customer, cb) => {
    const customerRef = db.collection('customers');
    cb(null, customer);
    // customerRef.add(customer)
    //     .then(ref => {
    //         cb(null, ref);
    //     }).catch(err => {
    //         cb({
    //             error: err.message,
    //             status: 500
    //         });
    //     });
}

const convertPassword = (customer, cb) => {
    try {
        var salt = bcrypt.genSaltSync(saltRounds);
        var hash = bcrypt.hashSync(customer.password, salt);
        customer.password = hash;
        cb(null, customer);
    }
    catch (err) {
        console.log(err.message);
    }
}

const findCustByPhone = (phone, cb) => {
    const customersRef = db.collection('customers');
    customersRef.where('phone', '==', phone).startAt().limit(1).get()
        .then(snapshot => {
            if (snapshot.size >= 1) {
                cb(null, snapshot.val());
            }
            cb(null, null)
        })
        .catch(err => cb({
            error: err.message,
            status: 500
        }));
}

const updateCustomerById = (id, customer, cb) => {
    const customerRef = db.collection('customers').doc(id);
    customerRef.update(customer)
        .then(ref => cb(null, ref))
        .catch(err => cb({
            error: err.message
        }));
}

// Tested using PostMan
customer_router.get("/retrieveCustomer/:id", async (req, res) => {
    const id = req.params.id;
    console.log("retrieving customer: ", id);
    try {
        const customerRef = await db.collection('customers').doc(id);
        customerRef.get().then(function (documentSnapshot) {
            if (documentSnapshot.exists) {
                const data = documentSnapshot.data();
                console.log("Document: ", data);
                res.send(data);
            } else {
                console.log('document not found');
                res.status(404).send({
                    error: "Document does not exist"
                });
            }
        });
    } catch (error) {
        console.log(error);
        res.status(404).send({
            error: "unable to locate customer"
        });
    }
});

// Tested using Postman
customer_router.get("/retrieveAll", async (req, res) => {
    try {
        const customersRef = await db.collection('customers');
        customersRef.where('live', '==', 1).get().then(snapshot => {
            const array = [];
            snapshot.forEach(doc => array.push(doc.data()));
            res.send(array);
        });
    } catch (error) {
        console.log(error);
        res.send(404, {
            error: "failed to locate customers"
        });
    }
});

customer_router.post('/sendSMS', check('phone').isMobilePhone(), async (req, res) => {
    Async.waterfall([
        (cb) => findCustByPhone(req.body.phone, cb),
        (customer, cb) => {
            const phoneCode = randomNumber({
                min: 999,
                max: 10000,
                integer: true
            });
            if (customer) {
                updateCustomerById(customer.id, Object.assign(customer, {
                    phoneCode: phoneCode
                }), cb)
            } else {
                addCustomer({
                    phone: req.body.phone,
                    phoneCode: phoneCode
                }, cb);
            }
        },
        (cust, cb) => {
            client.messages
                .create({
                    body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
                    from: '+15017122661',
                    to: cust.phone
                })
                .then(message => cb(null, cust))
                .done();
        }
    ], (err, result) => {
        if (err) {
            return res.status(err.status).json({
                error: err.message
            })
        }
        return res.json(result);
    })

});

customer_router.post('/checkSMS', [
    check('phone').isMobilePhone(),
    check('code').isNumeric(),
    check('code').isLength({
        min: 4,
        max: 4
    })
], async (req, res) => {
    findCustByPhone(req.body.phone, (err, customer) => {
        if (err) {
            res.status(res.status).json({
                error: error.error
            });
        }
        if (customer && customer.phoneCode && customer.phoneCode == req.body.code) {
            res.json({
                success: 'ok',
                customer: customer
            });
        } else {
            res.status(400).json({
                error: 'does not match phone code.'
            })
        }
    })
});

// Tested with Postman
customer_router.post("/registerCustomer", [
    check('email').isEmail(),
    check('password').isLength({
        min: 6
    }),
    check('phone').isMobilePhone()
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array()
        });
    }
    let customer = req.body;
    const customerRef = db.collection('customers');
    Async.waterfall([
        (cb) => {
            customerRef.where('email', '==', customer.email).get()
                .then(snapshot => {
                    if (snapshot.size >= 1) cb({
                        error: 'Email is already used.',
                        status: 400
                    });
                    cb(null)
                })
                .catch(err => {
                    cb({
                        error: err.message,
                        status: 500
                    });
                });
        },
        (cb) => convertPassword(customer, cb),
        (cust, cb) => addCustomer(cust, cb)
    ], (err, newCustomer) => {
        if (err)
            return res.status(err.status).json({
                error: err.error
            });
        return res.json(newCustomer);
    });
});

// Tested with Postman
customer_router.put("/updateCustomer/:id", async (req, res) => {
    const customer = req.body;
    const id = req.params.id;
    try {
        updateCustomerById(id, customer, (err, cust) => {
            if (err) {
                res.status(err.status).json({
                    error: err.error
                });
            } else {
                res.json({
                    success: 'ok',
                    customer: cust
                });
            }

        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error: error.message
        });
    }
});

// Tested with Postman
customer_router.delete("/deleteCustomer/:id", async (req, res) => {
    const id = req.params.id;
    try {
        const documentRef = await db.collection('customers').doc(id);
        await documentRef.delete();
        const confirmDelete = await documentRef.get().then(snapshot => {
            if (snapshot.exists) {
                return true;
            } else {
                return false;
            }
        });
        if (!confirmDelete) {
            res.sendStatus(200);
        } else {
            res.status(500).send({
                error: "Could not delete"
            });
        }
    } catch (error) {
        res.send(404, {
            error: "Failed"
        });
    }
});

module.exports = customer_router;