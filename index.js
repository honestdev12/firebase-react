// Firebase Requires
const firebase = require( 'firebase' );
const functions = require( 'firebase-functions' );
const firebase_admin = require( 'firebase-admin' );
const serviceAccount = require('./config/temp-firebase-server-firebase-adminsdk-hkwf2-5694da50c1.json');

// Firebase Config
const adminConfig = {
    apiKey: "AIzaSyDZmj3bgknrrp3-VJMKUOlhuwmxUtGQty0",
    authDomain: "temp-firebase-server.firebaseapp.com",
    databaseURL: "https://temp-firebase-server.firebaseio.com",
    projectId: "temp-firebase-server",
    storageBucket: "temp-firebase-server.appspot.com",
    credential: firebase_admin.credential.cert(serviceAccount)
};

// Firebase App Init
firebase_admin.initializeApp(adminConfig);

// Express App Requires
const express = require( 'express' );
const cors = require( 'cors' );
const bodyParser = require( 'body-parser' );

// Express App Init
const tedious_web_app = express();

// Server Port
const PORT = process.env.PORT || 5000;

// Express App Uses
tedious_web_app.use( express.static('public') ); // change to build before serving to client or deploying
tedious_web_app.use( cors( { origin: true } ) );
tedious_web_app.use( bodyParser.json() );
tedious_web_app.use( bodyParser.urlencoded( { extended: true } ) );
// Express Routers
/*
    Express Routers will be built here to be used by the express application

    Eventually we'll explore modularizing these routers
*/

// Customer Router
const customer_router = require("./routers/customer.router");
tedious_web_app.use("/customer", customer_router);

// Provider Router
const provider_router = require("./routers/provider.router");
tedious_web_app.use("/provider", provider_router);

// Task Router 
const task_router = require("./routers/task.router");
tedious_web_app.use("/task", task_router);

// Express App Listen
tedious_web_app.listen( PORT, () => {
    console.log( 'Magic happening on port: ', PORT );
} );

// Express App wrapped in Firebase Cloud Function Endpoint
module.exports = tedious_web_api = functions.https.onRequest( ( request, response ) => {
    if( !request.path ){
        request.url = `/${request.url}`;
    }
    return tedious_web_app( request, response );
} );