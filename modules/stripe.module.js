const stripe = require('stripe')(/*'STRIPE_API_KEY_HERE'*/);

let stripe_exports = {};

stripe_exports.createStripeCustomer = function(customer){
    stripe.customers.create(
    customer, // customer object | { description: "something", source: "SOURCE_TOKEN"}
    function(err, customer){

    });
}

stripe_exports.retrieveStripeCustomer = function(cu_id){
    stripe.customers.retrieve(
        cu_id, // Customer ID
        function(err, customer){

        }
    );
}

stripe_exports.updateStripeCustomer = function(cu_id, data){
    stripe.customers.update(
        cu_id, // Customer id
        data, // customer data with updates | { key: "updated value" }
        function(err, customer){

        }
    );
}

stripe_exports.deleteStripeCustomer = function(cu_id){
    stripe.customer.del(
        cu_id, // Customer id
        function(err, confirmation){

        }
    );
}

stripe_exports.createStripeCardForCustomer = function(cu_id, source){
    stripe.customers.createSource(
        cu_id, // Customer id
        source, // obtained via Stripe.js element | should be an object { source: SOURCE_TOKEN }
        function(err, card){

        }
    );
}

stripe_exports.retrieveStripeCardForCustomer = function(cu_id, ca_id){
    stripe.customers.retrieveCard(
        cu_id, // customer id
        ca_id, // card id
        function(err, card){

        }
    );
}

stripe_exports.updateStripeCardForCustomer = function(cu_id, ca_id, data){
    stripe.customers.updateCard(
        cu_id, // customer id
        ca_id, // card id
        data, // data to be updated
        function(err, card){

        }
    );
}

stripe_exports.deleteStripeCardForCustomer = function(cu_id, ca_id){
    stripe.customers.deleteCard(
        cu_id, // customer id
        ca_id, // card id
        function(err, confirmation){

        }
    );
}

stripe_exports.listAllStripeCardsForCustomer = function(cu_id){
    stripe.customers.listCards(
        cu_id, // customer id
        function(err, cards){
            
        }
    );
}

stripe_exports.createStripeCharge = function(charge){
    stripe.charges.create(
        charge, // object containing charge information | { amount: 1000, currency: "usd", source: "SOURCE TOKEN", description: "something" }
        function(err, charge){

        }
    );
}

stripe_exports.captureStripeCharge = function(charge_id){
    stripe.charges.capture(
        charge_id, // id of charge created previously
        function(err, charge){

        }
    );
}

module.exports = stripe_exports;